import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Camion } from './model/Camion';

@Injectable({
  providedIn: 'root'
})
export class CamionService {

  constructor(private http : HttpClient) { }

  url2="http://localhost:8080";
  //url2="https://transpro-test.appspot.com";

   getAllCamion(){
    return  this.http.get<Camion[]>(this.url2+"/listeCamion")
 
  }

  addCamion(item : Camion){

    let link=this.url2+"/ajouterCamion";
    let data = JSON.stringify(item);     
    
    let httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json'
         });    
         let options = {
      headers: httpHeaders
         };    
     return this.http.post<Camion>(link, data,options);
  }

  majCamion(item : Camion){

    let link=this.url2+"/majCamion";
    let data = JSON.stringify(item);     
    
    let httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json'
         });    
         let options = {
      headers: httpHeaders
         };    
     return this.http.put<Camion>(link, data,options);
  }

  suppCamion(id : string){

    let link=this.url2+"/suppCamion?id="+id;
     return this.http.delete(link);
  }


}
