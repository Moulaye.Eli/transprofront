import { Component, OnInit } from '@angular/core';
import { CamionService } from '../camion.service';
import { Camion } from '../model/Camion';



@Component({
  selector: 'app-camion',
  templateUrl: './camion.component.html',
  styleUrls: ['./camion.component.css']
})
export class CamionComponent implements OnInit {

  ajouter=false;
  lister=true;
  isMaj=false;
  camions : Camion[];
  camion ; Camion;
  isMatricule=false;

  constructor(private camionService : CamionService) {

    this.getAllCamion();
   }

getAllCamion(){
  this.camionService.getAllCamion()
       .subscribe(res =>{
        this.camions=res;
    }, err =>{

    }) 
}



  reset(){
    this.ajouter=false;
    this.lister=false;
    this.isMaj=false;
    this.isMatricule=false;

  
  }

  goTo(n){

    switch(n){
      case 1 : 
      this.reset();
      this.ajouter=true;
      break;
      case 2 :
      this.reset();
      this.lister=true;
      break;

    }

  }

  getMaj(item){
    this.camion=item;
    this.reset();
    this.isMaj=true;
  }

  majCamion(c){
    this.isMatricule=false;
    c.id=this.camion.id;
    this.camionService.majCamion(c)
    .subscribe(res =>{
      this.reset();
      this.majCamionItem(c);
      this.lister=true;
  }, err =>{
    let message=err.error.message;
    if(message=="matricule"){
       this.isMatricule=true;
    }
  }) 

  }

  suppCamion(c){

    this.camionService.suppCamion(c.id)
    .subscribe(res =>{
      this.suppCamionItem(c);
      this.reset();
      this.lister=true;
  }, err =>{
    
  }) 

  }

  addCamion(c){
    this.isMatricule=false;
    this.camionService.addCamion(c)
    .subscribe(res =>{
      this.camions.push(res);
      this.reset();
      this.lister=true;
  }, err =>{
      let message=err.error.message;
      if(message=="matricule"){
         this.isMatricule=true;
      }
  }) 

  }

  majCamionItem(item : Camion){
    this.camions.filter(c =>{
       c.id==item.id
    })
    .map(c =>{
      c==item
   })
    ;

  }

  suppCamionItem(item : Camion){
    console.log(item.id);
    console.log(this.camions);
    this.camions=this.camions.filter(c =>c!=item);
    console.log(this.camions);
  }

  ngOnInit() {
  }

}
