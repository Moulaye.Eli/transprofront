Comment installer et tester le projet

- installation de nodeJS  version >= 6.4.1 (vous pouvez telecharger la derniere version sur le site officiel nodejs.org)

- instalation d'Angular CLI sur le Terminal
$ npm install -g @angular/cli

- deplacer sur le repertoire du projet

- installer les dependances
$ npm install

- tester le projet
$ ng serve
